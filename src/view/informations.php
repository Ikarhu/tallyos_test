<html lang="fr" class="h-100">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Dev Authentification">
  <meta name="author" content="Baptiste David">

  <title>Informations</title>
  <!-- Bootstrap core CSS -->
  <link href="../../css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom styles for this template -->
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.css">
  <link href="../../css/style.css" rel="stylesheet">
  <link href="../../css/navbar.css" rel="stylesheet">


</head>
<body class="d-flex flex-column h-100">
  <?= $header ?>
  <br>
  <main class="flex-shrink-0">
    <div class="container">
        <!-- Tableau contenant toutes les ruches -->
      <table class="table hover mt-2">
       <thead>
        <tr>
          <th scope="col">Ruche</th>
          <th scope="col">Date</th>
          <th scope="col">Poids</th>
          <th scope="col">Température</th>
          <th scope="col">Humidité</th>
        </tr>
      </thead>
              <?php
        foreach ($getInformations as $inf) { ?>
          <tr>
<td><?= $inf['Hives_name'] ?></td>
<td><?= $inf['inf_date'] ?></td>
<td><?= $inf['inf_weight'] ?></td>
<td><?= $inf['inf_temp'] ?></td>
<td><?= $inf['inf_hum'] ?></td>
</tr>
       <?php } ?>
    </table>

  </div>
</main>
<?= $footer ?>
<script src="../../js/jquery.min.js"></script>
<script src="../../js/jquery.easing.min.js"></script>
<script src="../../js/bootstrap.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js"></script>
<script src="../../js/global.js"></script>
<script src="../../js/hive.js"></script>
</body>
</html>

