function editHive(id, name, latitude, longitude) {
	$("#editIdHive").val(id);
	$("#editHiveLabel").text("Editer la ruche " + name);
	$("#editNameHive").val(name);
	$("#editLat").val(latitude);
	$("#editLong").val(longitude);
	$('#modalEditHive').modal('show');
}

function delHive(id, name) {
	$("#delIdHive").val(id);
	$("#delHiveLabel").text("Supprimer la ruche " + name);
		$('#modalDelHive').modal('show')
}