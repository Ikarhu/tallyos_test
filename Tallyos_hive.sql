-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : sam. 20 juin 2020 à 17:38
-- Version du serveur :  10.1.44-MariaDB-0+deb9u1
-- Version de PHP : 7.3.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `Tallyos_hive`
--

-- --------------------------------------------------------

--
-- Structure de la table `Hives`
--

CREATE TABLE `Hives` (
  `Hives_id` int(11) NOT NULL,
  `Hives_name` varchar(11) NOT NULL,
  `Hives_lat` float NOT NULL,
  `Hives_long` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `Hives`
--

INSERT INTO `Hives` (`Hives_id`, `Hives_name`, `Hives_lat`, `Hives_long`) VALUES
(1, 'Ruche A', 12, 13),
(12, 'Ruche A\\\'', 12, 43);

-- --------------------------------------------------------

--
-- Structure de la table `Informations`
--

CREATE TABLE `Informations` (
  `inf_id` int(11) NOT NULL,
  `inf_hives_id` int(11) NOT NULL,
  `inf_date` datetime NOT NULL,
  `inf_weight` float NOT NULL,
  `inf_temp` int(11) NOT NULL,
  `inf_hum` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `Informations`
--

INSERT INTO `Informations` (`inf_id`, `inf_hives_id`, `inf_date`, `inf_weight`, `inf_temp`, `inf_hum`) VALUES
(1, 1, '2020-06-20 08:30:00', 43, 15, 78),
(2, 1, '2020-06-20 09:00:00', 43, 14, 78);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `Hives`
--
ALTER TABLE `Hives`
  ADD PRIMARY KEY (`Hives_id`);

--
-- Index pour la table `Informations`
--
ALTER TABLE `Informations`
  ADD PRIMARY KEY (`inf_id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `Hives`
--
ALTER TABLE `Hives`
  MODIFY `Hives_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT pour la table `Informations`
--
ALTER TABLE `Informations`
  MODIFY `inf_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
