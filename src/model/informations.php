<?php

class ModelInformations {
	public function getInformations() {
		require("../includes/config.php");
		$query = $dbh->prepare('SELECT Hives_name, inf_date, inf_weight, inf_temp, inf_hum FROM Hives INNER JOIN Informations ON inf_hives_id = Hives_id');
		$query->execute();
		$row = $query->fetchAll();
		return ($row);
	}
}

?>