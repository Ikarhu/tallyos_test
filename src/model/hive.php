<?php

class ModelHive {

	public function setHive($name, $latitude, $longitude) {
		require("../includes/config.php");

		if (strlen($name) > 65 || is_numeric($latitude) == false || is_numeric($longitude) == false) {
			return (84);
		} else {
			$name = addslashes ($name);
			$query = $dbh->prepare('INSERT INTO Hives (Hives_name, Hives_lat, Hives_long) VALUES (:name, :lat, :long)');
			$query->bindParam(':name', $name);
			$query->bindParam(':lat', $latitude);
			$query->bindParam(':long', $longitude);
			$query->execute();
		}
		return (0);
	}

	public function getHives() {
		require("../includes/config.php");
		$query = $dbh->prepare('SELECT * FROM Hives');
		$query->execute();
		$row = $query->fetchAll();
		return ($row);
	}

	public function editHive($id, $name, $latitude, $longitude) {
		require("../includes/config.php");

		if (strlen($name) > 65 || is_numeric($latitude) == false || is_numeric($longitude) == false) {
			return (84);
		} else {
			$name = addslashes ($name);
			$query = $dbh->prepare('UPDATE Hives SET Hives_name = :name, Hives_lat = :lat, Hives_long = :long WHERE Hives_id = :id');
			$query->bindParam(':name', $name);
			$query->bindParam(':lat', $latitude);
			$query->bindParam(':long', $longitude);
			$query->bindParam(':id', $id);
			$query->execute();
		}
		return (0);
	}

	public function delHive($id) {
		require("../includes/config.php");

		$query = $dbh->prepare('DELETE FROM Hives WHERE Hives_id = :id');
		$query->bindParam(':id', $id);
		$query->execute();
	}

}

?>