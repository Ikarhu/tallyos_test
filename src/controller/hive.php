<?php 
require("../model/hive.php");

$ModelHive = new ModelHive();

if ($_POST['postAddHive']) {
	$ret = $ModelHive->setHive($_POST['nameHive'],$_POST['lat'],$_POST['long']);
	if ($ret == 84) {
		$_SESSION['alert'] = "<div class='alert alert-danger alert-dismissible fade show'>Une erreur est survenue, veuillez entrer des données valides.<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
    <span aria-hidden='true'>&times;</span>
  </button></div>";
	}
	else {
		$_SESSION['alert'] = "<div class='alert alert-success alert-dismissible fade show'>La ruche a bien été ajoutée<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
    <span aria-hidden='true'>&times;</span>
  </button></div>";
	}
} else if ($_POST['postEditHive']) {
		$retEdit = $ModelHive->editHive($_POST['editIdHive'], $_POST['editNameHive'],$_POST['editLat'],$_POST['editLong']);
	if ($retEdit == 84) {
		$_SESSION['alert'] = "<div class='alert alert-danger alert-dismissible fade show'>Une erreur est survenue, veuillez entrer des données valides.<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
    <span aria-hidden='true'>&times;</span>
  </button></div>";
	}
	else {
		$_SESSION['alert'] = "<div class='alert alert-success alert-dismissible fade show'>La ruche a bien été éditée<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
    <span aria-hidden='true'>&times;</span>
  </button></div>";
	}
} else if ($_POST['postDelHive']) {
	$ModelHive->delHive($_POST['delIdHive']);
	$_SESSION['alert'] = "<div class='alert alert-success alert-dismissible fade show'>La ruche a bien été supprimée<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
    <span aria-hidden='true'>&times;</span>
  </button></div>";
}

$getHives = $ModelHive->getHives();
$header = file_get_contents('../includes/header.php');
$footer = file_get_contents('../includes/footer.php');

require_once("../view/hive.php");

?>
