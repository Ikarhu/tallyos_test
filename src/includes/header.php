  <header class="">
    <nav class="navbar navbar-expand-md navbar-light shadow-sm">
      <a class="navbar-brand" href="#"><img src="../../img/logo.png" alt="logo" class="img-link my-0 mr-md-auto logo"></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#idNavbar" aria-controls="idNavbar" aria-expanded="false" aria-label="Navbar">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="idNavbar">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link color-link" href="../../">Accueil <span class="sr-only"></span></a>
          </li>
          <li class="nav-item active">
            <a class="nav-link color-link" href="../controller/hive.php">Ruches <span class="sr-only"></span></a>
          </li>
          <li class="nav-item active">
            <a class="nav-link color-link" href="../controller/informations.php">Informations <span class="sr-only"></span></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>