<html lang="fr" class="h-100">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Dev Authentification">
  <meta name="author" content="Baptiste David">

  <title>Accueil</title>
  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet">
  <link href="css/navbar.css" rel="stylesheet">
</head>
<body class="d-flex flex-column h-100">
  <header class="">
    <nav class="navbar navbar-expand-md navbar-light shadow-sm">
      <a class="navbar-brand" href="#"><img src="img/logo.png" alt="logo" class="img-link my-0 mr-md-auto logo"></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#idNavbar" aria-controls="idNavbar" aria-expanded="false" aria-label="Navbar">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="idNavbar">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link color-link" href="#">Accueil <span class="sr-only"></span></a>
          </li>
          <li class="nav-item active">
            <a class="nav-link color-link" href="src/controller/hive.php">Ruches <span class="sr-only"></span></a>
          </li>
          <li class="nav-item active">
            <a class="nav-link color-link" href="src/controller/informations.php">Informations <span class="sr-only"></span></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <br>
  <main>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
          <div class="row">
            <div class="col-sm-2">
              <div class="circle"></div>
            </div>
            <div class="col-sm-10">
              <div class="form-group">
                <label for="templateName"><small class="form-text text-muted">Template Name</small></label>
                <input class="form-control" id="templateName">
                <label for="subject"><small class="form-text text-muted">Subject</small></label>
                <input class="form-control" id="subject">
              </div>
            </div>
            <div class="col-sm-12">
              <div class="form-group">
                <label for="exampleFormControlTextarea1"><small class="form-text text-muted">Message</small></label>
                <textarea class="form-control" id="message" rows="9"></textarea>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
          <div class="card" style="width: 18rem;">
            <img class="card-img-top" src="img/card-img.svg" alt="Card image cap">
            <div class="card-body">
              <h5 class="card-title">Card title</h5>
              <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
              <div class="row">
                <div class="col-sm-4">
              <a href="#" class="btn btn-primary">Button</a>
            </div>
                            <div class="col-sm-4">
  <button type="button" class="btn btn-outline-dark">Button</button>
            </div>
            </div>
            </div>
          </div>
        </div>
        <div class="col-sm-4">
          <small class="form-text text-muted">Message</small>
          <div class="dropdown">
            <button class="btn btn-block text-left btn-dropdown dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Email + Push
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="#">Option 1</a>
              <a class="dropdown-item" href="#">Option 2</a>
              <a class="dropdown-item" href="#">Option 3</a>
            </div>
          </div>
                    <small class="form-text text-muted">Send to group</small>

<div class="form-check">
  <input class="form-check-input" type="checkbox" value="" id="defaultCheck1" checked>
  <label class="form-check-label" for="defaultCheck1">
    Top Management
  </label>
</div>
<div class="form-check">
  <input class="form-check-input" type="checkbox" value="" id="defaultCheck2">
  <label class="form-check-label" for="defaultCheck2">
    Marketing Department
  </label>
</div>
<div class="form-check">
  <input class="form-check-input" type="checkbox" value="" id="defaultCheck3" checked>
  <label class="form-check-label" for="defaultCheck3">
    Design Department
  </label>
</div>
<div class="form-check">
  <input class="form-check-input" type="checkbox" value="" id="defaultCheck4">
  <label class="form-check-label" for="defaultCheck4">
    Financial Department
  </label>
</div>
<div class="form-check">
  <input class="form-check-input" type="checkbox" value="" id="defaultCheck5">
  <label class="form-check-label" for="defaultCheck5">
    Supply Department
  </label>
</div>
<div class="row mt-2">
  <div class="col-sm-3">
  <button type="button" class="btn btn-primary">Valider</button>
</div>
<div class="col-sm-3">
  <button type="button" class="btn btn-outline-dark">Annuler</button>
  </div>
</div>
        </div>
      <div class="col-sm-4">
      </div>
      <div class="col-sm-4">
            <small class="form-text text-muted">Tap target</small>
                      <div class="dropdown">
            <button class="btn btn-block text-left btn-dropdown dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
             Profile Screen
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="#">Option 1</a>
              <a class="dropdown-item" href="#">Option 2</a>
              <a class="dropdown-item" href="#">Option 3</a>
            </div>
          </div>
            <small class="form-text text-muted">Set Type</small>
          <div class="form-check mt-1">
  <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1">
  <label class="form-check-label" for="exampleRadios1">
    <span class="badge badge-success">News</span>
  </label>
</div>
<div class="form-check mt-1">
  <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2" checked>
  <label class="form-check-label" for="exampleRadios2">
    <span class="badge badge-info">Reports</span>
  </label>
</div>
<div class="form-check mt-1">
  <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios3" value="option3">
  <label class="form-check-label" for="exampleRadios3">
    <span class="badge badge-warning">Documents</span>
  </label>
</div>
<div class="form-check mt-1">
  <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios4" value="option4">
  <label class="form-check-label" for="exampleRadios4">
    <span class="badge badge-primary">Media</span>
  </label>
</div>
<div class="form-check mt-1">
  <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios5" value="option5">
  <label class="form-check-label" for="exampleRadios5">
    <span class="badge badge-secondary">Text</span>
  </label>
</div>
<button type="button" class="btn btn-danger mt-2">Supprimer</button>

      </div>
      </div>

    </div>
  </div>
</main>
<div class="footer bg-footer mt-auto py-3">
  <p>&copy; 2020, Baptiste DAVID</p>
</div>

<script src="js/jquery.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap.bundle.js"></script>
<script src="js/index.js"></script>
</body>
</html>

