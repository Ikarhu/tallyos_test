**Test Technique - Hive**

Cette application web fut codée lors d'un test technique suite à un entretien téléphonique.
Le but était de créér une plateforme web capable d'ajouter, supprimer et modifier des ruches. 
De plus, toutes les informations que les ruches envoyées dans la base de données MySql devait être visible sur la page "Informations".
La page accueil était un bonus à faire, pour montrer les différentes techniques de CSS utilisables.

Ce test a été effectué en une dizaine d'heures.

Compétences requises: Html, Css, Php, Javascript, Mysql

Le site est accessible directement à cette adresse: https://bigjoeminecraft.websr.fr/Tallyos/