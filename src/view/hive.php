<html lang="fr" class="h-100">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Dev Authentification">
  <meta name="author" content="Baptiste David">

  <title>Ruches</title>
  <!-- Bootstrap core CSS -->
  <link href="../../css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom styles for this template -->
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.css">
  <link href="../../css/style.css" rel="stylesheet">
  <link href="../../css/navbar.css" rel="stylesheet">


</head>
<body class="d-flex flex-column h-100">
  <?= $header ?>
  <br>
  <main class="flex-shrink-0">
    <div class="container">
      <?php if ($_SESSION['alert']) {
    echo $_SESSION['alert']; } 
    $_SESSION['alert'] = "";
    ?>

        <!-- Bouton pour lancer la modal -->
      <button type="button" data-toggle="modal" data-target="#modalAddHive" class="btn btn-success mb-2">Ajouter une ruche</button>

        <!-- Tableau contenant toutes les ruches -->
      <table class="table hover mt-2">
       <thead>
        <tr>
          <th scope="col">Nom</th>
          <th scope="col">Latitude</th>
          <th scope="col">Longitude</th>
          <th scope="col"></th>
        </tr>
      </thead>
              <?php
        foreach ($getHives as $hive) { ?>
          <tr>
<td><?= stripslashes ($hive['Hives_name']) ?></td>
<td><?= $hive['Hives_lat'] ?></td>
<td><?= $hive['Hives_long'] ?></td>
<td><a class="text-primary btn-a" onclick="editHive(<?= $hive['Hives_id'] ?>, '<?= $hive['Hives_name'] ?>',<?= $hive['Hives_lat'] ?>,<?= $hive['Hives_long'] ?>)">Modifier</a> / <a class="text-primary btn-a" onclick="delHive(<?= $hive['Hives_id'] ?>, '<?= $hive['Hives_name'] ?>')">Supprimer</a></td>
</tr>
       <?php } ?>
    </table>

  </div>


  <!-- Modal Ajout d'une ruche -->
    <div class="modal fade" id="modalAddHive" tabindex="-1" role="dialog" aria-labelledby="addHiveLabel" aria-hidden="true">
        <form id="addHive" method="post">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="addHiveLabel">Ajouter une ruche</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <label for="nameHive">Nom de la ruche</label>
              <input class="form-control" id="nameHive" name="nameHive" placeholder="Saisir le nom de la ruche" required>
            </div>
            <div class="form-group">
              <label for="lat">Latitude</label>
              <input class="form-control" id="lat" name="lat" placeholder="Saisir la latitude de la ruche" required>
            </div>
            <div class="form-group">
              <label for="long">Longitude</label>
              <input class="form-control" id="long" name="long" placeholder="Saisir la longitude de la ruche" required>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
            <button type="submit" class="btn btn-success" name="postAddHive" value="postAddHive">Ajouter</button>
          </div>
        </div>
      </div>
        </form>
    </div>

  <!-- Modal Ajout d'une ruche -->
    <div class="modal fade" id="modalEditHive" tabindex="-1" role="dialog" aria-labelledby="editHiveLabel" aria-hidden="true">
        <form id="editHive" method="post">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="editHiveLabel">Editer une ruche</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <label for="nameHive">Nom de la ruche</label>
              <input class="form-control" id="editNameHive" name="editNameHive" placeholder="Saisir le nom de la ruche" required>
            </div>
            <div class="form-group">
              <label for="lat">Latitude</label>
              <input class="form-control" id="editLat" name="editLat" placeholder="Saisir la latitude de la ruche" required>
            </div>
            <div class="form-group">
              <label for="long">Longitude</label>
              <input class="form-control" id="editLong" name="editLong" placeholder="Saisir la longitude de la ruche" required>
            </div>
            <input hidden id="editIdHive" name="editIdHive" value="-1">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
            <button type="submit" class="btn btn-success" name="postEditHive" value="postEditHive">Editer</button>
          </div>
        </div>
      </div>
        </form>
    </div>

  <!-- Modal Ajout d'une ruche -->
    <div class="modal fade" id="modalDelHive" tabindex="-1" role="dialog" aria-labelledby="delHiveLabel" aria-hidden="true">
        <form id="delHive" method="post">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="delHiveLabel">Supprimer une ruche</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            Voulez vous supprimer cette ruche ?
            <input hidden id="delIdHive" name="delIdHive" value="-1">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
            <button type="submit" class="btn btn-danger" name="postDelHive" value="postDelHive">Supprimer</button>
          </div>
        </div>
      </div>
        </form>
    </div>

</main>
<?= $footer ?>
<script src="../../js/jquery.min.js"></script>
<script src="../../js/jquery.easing.min.js"></script>
<script src="../../js/bootstrap.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js"></script>
<script src="../../js/global.js"></script>
<script src="../../js/hive.js"></script>
</body>
</html>

